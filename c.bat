@echo off
cls

cd src
ghc -O -outputdir "../build" -o "../main.exe" Main.hs
cd ..