import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Data.Maybe
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State
import Control.Monad.Except

import Util

data TypeVal
  = TypeProp
  | TypeSet
  | TypeIdent N
  | TypeFunc Type Type
  | TypeLam Type
  deriving (Eq, Ord)

show_type_val :: TypeVal -> String
show_type_val v = case v of
  TypeProp -> "TypeProp"
  TypeSet -> "TypeSet"
  TypeIdent n -> unwords
    ["TypeIdent", show n]
  TypeFunc t1 t2 -> unwords
    ["TypeFunc", pshow t1, pshow t2]
  TypeLam t -> unwords
    ["TypeLam", pshow t]

instance Show TypeVal where
  show = show_type_val

type TypeCtx = Set N

show_type_ctx :: TypeCtx -> String
show_type_ctx c = show_set show c

data Type = Type TypeCtx N TypeVal
  deriving (Eq, Ord)

-- show_type :: Type -> String
-- show_type (Type c k v) = unwords
--   [ "Type"
--   , show_type_ctx c
--   , show k
--   , prns # show v
--   ]

show_type :: Type -> String
show_type (Type c k v) = show v

instance Show Type where
  show = show_type

type_prop :: Type
type_prop = Type Set.empty 0 TypeProp

type_set :: Type
type_set = Type Set.empty 0 TypeSet

type_ident :: N -> Type
type_ident n = Type (Set.singleton n) 0 #
  TypeIdent n

type_func :: Type -> Type -> Type
type_func a@(Type c1 0 v1) b@(Type c2 0 v2) =
  Type (Set.union c1 c2) 0 # TypeFunc a b

type_lam :: Type -> Type
type_lam a@(Type c k v) =
  let !c1 = Set.map dec # Set.delete 0 c
  in Type c1 (k + 1) # TypeLam a

type_app :: Type -> Type -> Type
type_app a@(Type c1 k1 (TypeLam t)) b@(Type c2 0 v2) =
  asrt undefined (k1 /= 0) # type_subst b 0 t

type_subst :: Type -> N -> Type -> Type
type_subst x@(Type _ _ _) k t@(Type c _ v) =
  if not # Set.member k c then type_lift (-1) k t
  else case v of
    TypeIdent n -> if n < k then t
      else if n > k then type_ident # n - 1
      else type_lift k 0 x
    TypeFunc t1 t2 -> type_func (type_subst x k t1) (type_subst x k t2)
    TypeLam t1 -> type_lam # type_subst x (k + 1) t1

type_lift :: N -> N -> Type -> Type
type_lift n k t@(Type c _ v) =
  if Set.null c then t else case v of
    TypeIdent m -> if m < k then t else
      let !x = m + n
      in asrt (error # show (n, k, t)) (x >= 0) # type_ident x
    TypeFunc t1 t2 -> type_func (type_lift n k t1) (type_lift n k t2)
    TypeLam t1 -> type_lam # type_lift n (k + 1) t1

check_type :: Type -> Maybe Type
check_type t@(Type _ _ _) = Just t

-----

data TermVal
  = TermImp
  | TermUniv
  | TermMem
  | TermTau
  | TermAxiomCnd
  | TermIdent Type N
  | TermLam Type Term
  | TermApp Term Term
  | TermTypeLam Term
  | TermTypeApp Term Type
  deriving (Eq, Ord)

show_term_val :: TermVal -> String
show_term_val v = case v of
  TermImp -> "TermImp"
  TermUniv -> "TermUniv"
  TermMem -> "TermMem"
  TermTau -> "TermTau"
  TermAxiomCnd -> "TermAxiomCnd"
  TermIdent t n -> unwords
    ["TermIdent", pshow t, show n]
  TermLam t e -> unwords
    ["TermLam", pshow t, pshow e]
  TermApp e1 e2 -> unwords
    ["TermApp", pshow e1, pshow e2]
  TermTypeLam e -> unwords
    ["TermTypeLam", pshow e]
  TermTypeApp t e -> unwords
    ["TermTypeApp", pshow t, pshow e]

instance Show TermVal where
  show = show_term_val

type TermCtx = Map N Type

show_term_ctx :: TermCtx -> String
show_term_ctx c = show_set <~ map_to_set c #
  \(n, t) -> concat [show n, " -> ", show t]

data Term = Term TypeCtx TermCtx Type TermVal
  deriving (Eq, Ord)

-- show_term :: Term -> String
-- show_term (Term ct ce t v) = unwords
--   [ "Term"
--   , show_type_ctx ct
--   , show_term_ctx ce
--   , prns # show t
--   , prns # show v
--   ]

show_term :: Term -> String
show_term (Term ct ce t v) = show v

instance Show Term where
  show = show_term

term_imp :: Term
term_imp = Term Set.empty Map.empty
  (type_func type_prop # type_func type_prop type_prop)
  TermImp

term_univ :: Term
term_univ = Term Set.empty Map.empty
  (type_lam #
    type_func (type_func (type_ident 0) type_prop) type_prop)
  TermUniv

term_mem :: Term
term_mem = Term Set.empty Map.empty
  (type_func type_set # type_func type_set type_prop)
  TermMem

term_tau :: Term
term_tau = Term Set.empty Map.empty
  (type_lam #
    type_func (type_func (type_ident 0) type_prop) # type_ident 0)
  TermTau

term_ident :: Type -> N -> Term
term_ident t@(Type c 0 _) n =
  Term c (Map.singleton n t) t # TermIdent t n

term_lam :: Type -> Term -> Term
term_lam t@(Type ct 0 _) e@(Term cte ce te@(Type _ 0 _) v) =
  let !t' = Map.lookup 0 ce
  in asrt undefined (t' == Nothing || t' == Just t) # let
    !cte1 = Set.union cte ct
    !ce1 = map_shift ce
    !(Just t1) = check_type # type_func t te
    !dflt = Term cte1 ce1 t1 # TermLam t e
    in case v of
      TermApp e1@(Term _ ce1 _ _) (Term _ _ _ (TermIdent _ 0)) ->
        if not # Map.member 0 ce1 then term_lift (-1) 0 e1 else dflt
      _ -> dflt

term_app :: Term -> Term -> Term
term_app
  e1@(Term cte1 ce1 t@(Type _ 0 (TypeFunc t1 t2)) v1)
  e2@(Term cte2 ce2 t1'@(Type _ 0 _) v2) =
    asrt undefined (t1 == t1') # let
    !cte = Set.union cte1 cte2
    !(Just ce) = conj_union ce1 ce2
    in case v1 of
      TermLam _ e -> term_subst e2 0 e
      _ -> Term cte ce t2 # TermApp e1 e2

term_type_lam :: Term -> Term
term_type_lam e@(Term _ ce t _) =
  asrt undefined (Map.null ce) # let
    !t1@(Type !c _ _) = type_lam t
    in Term c Map.empty t1 # TermTypeLam e

term_type_app :: Term -> Type -> Term
term_type_app e@(Term cte ce te _) t@(Type ct _ _) =
  asrt undefined (Map.null ce) # let
    !(Just t1) = check_type # type_app te t
    !c = Set.union cte ct
    in Term c Map.empty t1 # TermTypeApp e t

term_subst :: Term -> N -> Term -> Term
term_subst x@(Term ctx cex tx vx) k e@(Term cte cee te v) =
  if not # Map.member k cee then term_lift (-1) k e
  else case v of
    TermIdent _ n -> if n < k then e
      else if n > k then term_ident te # n - 1
      else term_lift k 0 x
    TermLam t1 e1 -> term_lam t1 # term_subst x (k + 1) e1
    TermApp e1 e2 -> term_app (term_subst x k e1) (term_subst x k e2)

term_lift :: N -> N -> Term -> Term
term_lift n k e@(Term cte cee te v) =
  if Map.null cee then e else case v of
    TermIdent _ m -> if m < k then e else
      let !x = m + n
      in asrt (error # show (n, k, e)) (x >= 0) # term_ident te x
    TermLam t e1 -> term_lam t # term_lift n (k + 1) e1
    TermApp e1 e2 -> term_app (term_lift n k e1) (term_lift n k e2)

check_term :: Term -> Maybe Term
check_term e@(Term _ _ _ _) = Just e

-----

check_stat :: Term -> Maybe Term
check_stat e@(Term _ _ t _) = do
  True <- pure # t == type_prop
  return e

-----

type ProofCtx = Map N Term

show_proof_ctx :: ProofCtx -> String
show_proof_ctx c = show_set <~ map_to_set c #
  \(n, e) -> concat [show n, " -> ", show e]

data Proof = Proof TypeCtx TermCtx ProofCtx Term
  deriving (Eq, Ord)

show_proof :: Proof -> String
show_proof (Proof ct ce cp s) = let
  !s1 = show s
  in concat ["Proof ", put_in_parens' (elem space s1) s1]

instance Show Proof where
  show = show_proof

proof_axiom :: Proof
proof_axiom = Proof Set.empty Map.empty Map.empty #
  Term Set.empty Map.empty type_prop TermAxiomCnd

proof_ident :: Term -> N -> Proof
proof_ident s n = case check_stat s of
  Just (Term ct ce _ _) -> Proof ct ce (Map.singleton n s) s

proof_lam :: Term -> Proof -> Proof
proof_lam s p@(Proof ctp cep cpp sp) = case check_stat s of
  Just (Term cts ces _ _) -> let
    !s' = Map.lookup 0 cpp
    in asrt undefined (s' == Nothing || s' == Just s) # let
      !ct = Set.union cts ctp
      !(Just ce) = conj_union ces cep
      !cp = map_shift cpp
      !s1 = term_app (term_app term_imp s) sp
      in Proof ct ce cp s1

proof_app :: Proof -> Proof -> Proof
proof_app
  p1@(Proof ctp1 cep1 cpp1 s)
  p2@(Proof ctp2 cep2 cpp2 s1) = let
    !(Term _ _ _ !(TermApp !s_aux !s2)) = s
    !(Term _ _ _ !(TermApp !(Term _ _ _ !TermImp) !s1)) = s_aux
    !ct = Set.union ctp1 ctp2
    !(Just ce) = conj_union cep1 cep2
    !(Just cp) = conj_union cpp1 cpp2
    in Proof ct ce cp s2

proof_term_lam :: Type -> Proof -> Proof
proof_term_lam t@(Type ct 0 _) p@(Proof ctp cep cpp s) =
  let f_cnd (Term _ ce _ _) = not # Map.member 0 ce
  in asrt undefined (all f_cnd # Map.elems cpp) # let
    !(Just f) = check_term # term_lam t s
    !(Just s1) = check_term # term_app (term_type_app term_univ t) f
    !ctp1 = Set.union ctp ct
    !cep1 = map_shift cep
    !cpp1 = Map.map (term_lift (-1) 0) cpp
    in Proof ctp1 cep1 cpp1 s1

proof_term_app :: Proof -> Term -> Proof
proof_term_app p@(Proof ctp cep cpp s) e@(Term cte cee t _) = let
  !(Term _ _ _ !(TermApp aux1 f)) = s
  !(Term _ _ _ !(TermTypeApp aux2 t')) = aux1
  !(Term _ _ _ !TermUniv) = aux2
  in asrt undefined (t == t') # let
    !ctp1 = Set.union ctp cte
    !(Just cep1) = conj_union cep cee
    !s1 = term_app f e
    in Proof ctp1 cep1 cpp s1

proof_type_lam :: Proof -> Proof
proof_type_lam p@(Proof _ cep cpp s) =
  asrt undefined (Map.null cep && Map.null cpp) # let
  !s1@(Term !c _ _ _) = term_type_lam s
  in Proof c Map.empty Map.empty s1

proof_type_app :: Proof -> Type -> Proof
proof_type_app p@(Proof ctp cep cpp s) t@(Type ct _ _) =
  asrt undefined (Map.null cep && Map.null cpp) # let
  !s1@(Term !c1 _ _ _) = term_type_app s t
  !c = Set.union ctp c1
  in Proof c Map.empty Map.empty s1

imp_i = proof_lam
imp_e = proof_app
univ_i = proof_term_lam
univ_e = proof_term_app

-----

main :: IO ()
main = let
  -- true    := V P, P -> P
  -- and P Q := V R, R P Q -> R Q true
  -- left    := V P Q, and P Q -> P
  -- right   := V P Q, and P Q -> Q

  x0 = term_ident type_prop 0
  x1 = term_ident type_prop 1
  x2 = term_ident type_prop 2

  true = mk_univ type_prop # mk_imp x0 x0

  t = type_func type_prop # type_func type_prop type_prop
  r = term_ident t 0
  and' = term_lam type_prop # term_lam type_prop # mk_univ t #
    mk_imp (app [r, x2, x1]) # app [r, x1, true]
  
  left' = mk_univ type_prop # mk_univ type_prop #
    mk_imp (app [and', x1, x0]) x1
  right' = mk_univ type_prop # mk_univ type_prop #
    mk_imp (app [and', x1, x0]) x0

  triv = univ_i type_prop # imp_i x0 # proof_ident x0 0

  left = triv

  target = left'
  p = left
  
  ok = proof_stat p == target &&
    (Set.null # proof_type_ctx p) &&
    (Map.null # proof_term_ctx p) &&
    (Map.null # proof_proof_ctx p)
  
  in do
    print # proof_type_ctx p
    print # proof_term_ctx p
    print # proof_proof_ctx p
    putStrLn ""
    print target
    putStrLn ""
    print # proof_stat p
    logb
    putStrLn # ite ok "OK" "/"

app :: [Term] -> Term
app = foldl1 term_app

mk_imp :: Term -> Term -> Term
mk_imp a b = app [term_imp, a, b]

mk_univ :: Type -> Term -> Term
mk_univ t e = term_app (term_type_app term_univ t) #
  term_lam t e

term_type :: Term -> Type
term_type (Term _ _ t _) = t

term_type_ctx :: Term -> TypeCtx
term_type_ctx (Term ct _ _ _) = ct

proof_stat :: Proof -> Term
proof_stat (Proof _ _ _ s) = s

proof_type_ctx :: Proof -> TypeCtx
proof_type_ctx (Proof ct _ _ _) = ct

proof_term_ctx :: Proof -> TermCtx
proof_term_ctx (Proof _ ce _ _) = ce

proof_proof_ctx :: Proof -> ProofCtx
proof_proof_ctx (Proof _ _ cp _) = cp

-----

asrt :: () -> Prop -> a -> a
asrt _ True = id
asrt () False = id

conj_union :: (Ord k, Eq v) => Map k v -> Map k v -> Maybe (Map k v)
conj_union mp1 mp2 = map_fold_r mp2 (pure mp1) # \k v acc -> do
  mp <- acc
  case Map.lookup k mp of
    Nothing -> pure # Map.insert k v mp
    Just v' -> do
      True <- pure # v == v'
      return mp

map_shift :: Map N v -> Map N v
map_shift mp = Map.fromList # Map.toList mp >>=
    \(k, v) -> if k == 0 then [] else [(k - 1, v)]

pshow :: (Show a) => a -> String
pshow = prns . show

prns :: String -> String
prns s = if elem space s then put_in_parens s else s